# PageAuthors

Mediawiki extension.

## Description

* Read authors from history page and display in the article.
* Only for main namespace.
* Not for main page.
* Insert `<pageauthors>Custom author</pageauthors>` to override default settings.
* Insert `<pageauthors></pageauthors>` to hide authors.

## Installation

* Make sure you have MediaWiki 1.29+ installed.
* Download and place the extension to your /extensions/ folder.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'PageAuthors' )`;

## Configuration

* Set _bestAuthorsCount_ in _extensions.json_. It's the number of authors with highest count of added characters.

## Internationalization

This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2018 Nugis Finem, z.s.
