<?php

/**
 * All hooked functions used by PageAuthors
 * @ingroup Extensions
 * @author Josef Martiňák
 * @license MIT
 * @file
 */

class PageAuthorsHooks {
	

	/**
	 * Place the box with page authors
	 * @param object $out: instance of OutputPage
	 * @param object $skin: instance of Skin, unused
	 */
	public static function showAuthors( &$out, &$skin ) {

		// doesn't work for Minerva
		if($skin->getSkinName()=='minerva') return true;
		if ( $out->isArticle() ) {

			$title = $out->getTitle();
			if( $title->isMainPage() || $title->getNamespace() != 0) return true;
			$out->addModules( 'ext.PageAuthors' );

			if(preg_match("/&lt;pageauthors&gt;(.*?)&lt;\/pageauthors&gt;/", $out->mBodytext, $m)) {
				// override default behaviour; set authors directly
				$out->mBodytext = preg_replace("/&lt;pageauthors&gt;(.*?)&lt;\/pageauthors&gt;/", "", $out->mBodytext);
				if(!empty($m[1])) {
					$out->addWikiText("<div id='pgAuthors'><b>". $out->msg('pageauthors-header')->text() . "</b>: " . $m[1] . "</div>");
				}
				return true;
			}

			$config = $out->getConfig();
			$user = $out->getUser();

			$wikipage = WikiPage::factory( $out->getTitle() );

			$revision = $wikipage->getRevision();

			// page doesn't exist
			if(empty($revision)) return true;

			$userArr = array();
			
			$uname = $revision->getUserText();
			$size = $revision->getSize();

			while($revision = $revision->getPrevious()) {
				$sizePrev = $revision->getSize();
				$diff = $size - $sizePrev;
				
				if(array_key_exists($uname, $userArr)) $userArr[$uname] += $diff;
				else $userArr[$uname] = $diff;

				$uname = $revision->getUserText();			
				$size = $sizePrev;
			}
			if(array_key_exists($uname, $userArr)) $userArr[$uname] += $size;
			else $userArr[$uname] = $size;

			var_dump($userArr);
			uasort($userArr, 'self::cmp');
			var_dump($userArr);

			$output = "<div id='pgAuthors'><b>". $out->msg('pageauthors-header')->text() . "</b>: ";
			foreach($userArr as $key=>$value) {
				$userRealName = $user->whoIsReal( $user->idFromName($key) );
				if(!empty($userRealName)) {
					$output .= "[[User:$key|$key]] ($userRealName), ";
				}
				else $output .= "[[User:$key|$key]], ";
			}
			$output = rtrim($output, ' ,');
			$out->addWikiText($output);
			$out->addHTML("</div>");
		}
		return true;
	}


	/**
	 * Comparison function
	 */
	function cmp($a, $b) {
    	if ($a == $b) {
        	return 0;
    	}
    	return ($a < $b) ? 1 : -1;
	}
}